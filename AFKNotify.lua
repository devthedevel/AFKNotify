--[[ 
Addon Title: AFK Notify
Addon For: The Elder Scrolls Online
Author: Dev909
Description: Small addon for The Elder Scrolls Online that notifies you who whispered you while AFK.
Version: 1.0
Date Created: April 8th, 2014
Contact: dfcm15@mun.ca
Filenames: AFKNotify.lua
           AFKNotify.txt
           README.txt
           LICENSE
Comments: My code is pretty much allllll over the place. Once I get it working and comfortable with it, I'll clean it up.

Copyright: Copyright 2014, Devin Marsh
Copyright Statement:
    This file is part of AFK Notify.

    AFK Notify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AFK Notify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFK Notify.  If not, see <http://www.gnu.org/licenses/>.
]]--


local VERSION = "1.0"
local AUTHOR = "Dev909"
local afk = false
local found = false
local count = 0
local c = 0
local k, h

local notifyArray = {}
for i = 1, 100 do
notifyArray[i] = ""
end

local trimmedArray = {}
for i = 1, 100 do
trimmedArray[i] = ""
end

local notifyCount = {}
for i = 1, 100 do
notifyCount[i] = 0
end

function setAFK(msg)
if (msg == "about" or msg == "help" or msg == "a" or msg == "h") then
d ( "[AFK Notify created by " .. AUTHOR .. ", Version: " .. VERSION .. "]\nAFK Notify is a lightweight chat addon that notifies you who whispered you. This is useful for instantly knowing you whispered you, instead of searching the chat logs.\nTo activate type /afk\nType /afk again to return to online status")
else
if (afk == false) then
  afk = true
  count = 0
  d( "You are now AFK")
elseif (afk == true) then
  if (count == 0) then
    d ( "You are no longer AFK\nYou have received no messages while away")
  else
    afk = false
    trimCount(notifyArray, count)
    d( "You are no longer AFK")
    d( "You have received:")
    c = c + 1
    for i = 1, c do
      d ( notifyCount[i] .. " messages from " .. trimmedArray[i])
      notifyArray[i] = ""
      notifyCount[i] = 0
      trimmedArray[i] = ""
    end
    c = 0
    count = 0
  end
end
end
end

function trimCount(array, count)
local old, new;
found = false
c = 0
  for x = 1, count do
    old = array[x]
    y = 0
    h = 0
    found = false
    while (y <= count and found == false) do
      for z = 0, count do
        if (trimmedArray[z] == array[x]) then
          found = true
        else
          y = y + 1
          new = array[y]
          if (old == new) then
            h = h + 1
            notifyCount[c+1] = h
          end
        end
      end
    end
    if (found == false) then
      c = c + 1
      trimmedArray[c] = array[x]
    end
  end
  
  for x = 1, 100 do
    trimmedArray[x] = ""
  end
  trimmedArray[1] = array[1]
  local y = 0
  c = 0
  for x = 1, count do
    found = false
    y = 0
    while (y <= count and found == false) do
        y = y + 1
          for z = 0, count do
            if (trimmedArray[y+z] == array[x]) then
              found = true
            end
          end
          if (found == false) then
            c = c + 1
            trimmedArray[y+c] = array[x]
            found = true            
          end
       end
    end
end


function incMessage(id, channel, fromName, msg)
  if (afk == true and channel == CHAT_CHANNEL_WHISPER) then
    count = count + 1
    local name = ZO_LinkHandler_CreatePlayerLink(fromName)
    notifyArray[count] = name
  end
end

local function OnAddOnLoaded(eventCode, addOnName)
    if(addOnName ~= "AFK Notify") then
      return
    end
    d ( "AFK Notify loaded. Type /afk help for more information ")
end

EVENT_MANAGER:RegisterForEvent("AFKNotify", EVENT_CHAT_MESSAGE_CHANNEL, incMessage)
EVENT_MANAGER:RegisterForEvent("AFKNotify", EVENT_ADD_ON_LOADED, OnAddOnLoaded)

SLASH_COMMANDS["/afk"] = setAFK
