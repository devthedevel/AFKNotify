Addon Title: AFK Notify
Addon For: The Elder Scrolls Online
Author: Dev909 (Devin Marsh)
Description: Small addon for The Elder Scrolls Online that notifies you who whispered you while AFK.
Version: 1.0
Date Created: April 8th, 2014
Contact: dfcm15@mun.ca
Filenames: AFKNotify.lua
		   AFKNotify.txt
		   README.txt
		   LICENSE
Usage: Type [/afk help] for in game help.
	   Type [/afk] to activate. Type [/afk] again to show any users that sent you a private message, and how many messages were sent in total. 
Copyright: Copyright 2014, Devin Marsh
Copyright Statement:
    This file is part of AFK Notify.

    AFK Notify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AFK Notify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFK Notify.  If not, see <http://www.gnu.org/licenses/>.
